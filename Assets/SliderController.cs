﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour {
	[HideInInspector]
	public Slider ScoreSlider;
	public GameObject LosePopUp;
	public float MinMinutes;
	public float MinSeconds;
	public bool TimeBasedLevel;

	[HideInInspector]
	public float maxValue;
	float valueSlice;
	float timeCounter = 1;

	private void Awake() {
	ScoreSlider = GetComponent<Slider>();
	}

	private void Start() {
		valueSlice = 1/((MinMinutes*60)+MinSeconds);
	}

	private void Update() {
		if(GlobalFlag.isPlaying){
			timeCounter -= Time.deltaTime;
			if(timeCounter <= 0){
				ScoreSlider.value -= valueSlice;
				if(ScoreSlider.value <= 0 && TimeBasedLevel){ // untuk yang objectivenya before time runs out
					GlobalFlag.isPlaying = false;
					LosePopUp.SetActive(true);
				}
				timeCounter = 1;
			}	
		}
	}
}
