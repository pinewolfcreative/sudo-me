﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneManager : MonoBehaviour {

	public static SceneManager instance;
	Scene currentScene;
	public static int prevBuildIndex;
	private void Awake() {
		instance = this;
		currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
	}
	public void GoToScene(string SceneName){
		prevBuildIndex = currentScene.buildIndex;
		UnityEngine.SceneManagement.SceneManager.LoadScene(SceneName);
	}

	public void Back(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(prevBuildIndex);
	}

	public void Play(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(LevelDatabase.CurrentLevel);
	}

	public void Retry(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(currentScene.name);
	}
	private void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)){
			switch (currentScene.name)
			{
				case "01HomeScene":
					Application.Quit();
				break;
				case "02LevelSelection":
					GoToScene("01HomeScene");
				break;
				default:
				break;
			}
		}
	}

	public static string GetCurrentSceneName(){
		return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
	}
}
