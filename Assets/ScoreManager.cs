﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScoreManager : MonoBehaviour {

	public static ScoreManager instance;
	public static int StarCurrentLevel;
	public SliderController SliderController;
	float _2per3maxvalue;
	float _1per3maxvalue;


	private void Awake() {
		instance = this;
	}
	private void Start() {
		_2per3maxvalue = (2f/3f);
		_1per3maxvalue = (1f/3f);
	}

	public void UpdateScore(){
		string levelName = LevelDatabase.CurrentLevel;
		float sliderValue = SliderController.ScoreSlider.value;

		if(sliderValue >= _2per3maxvalue){
			StarCurrentLevel = 3;
			if(PlayerPrefs.GetInt(levelName) <= 3){
				PlayerPrefs.SetInt(levelName, 3);
			}
			
		} else if(sliderValue >= _1per3maxvalue && sliderValue <= _2per3maxvalue){
			StarCurrentLevel = 2;
			if(PlayerPrefs.GetInt(levelName) <= 2){
				PlayerPrefs.SetInt(levelName, 2);
			}
			
		} else if(sliderValue < _1per3maxvalue && sliderValue > 0){
			StarCurrentLevel = 1;
			if(PlayerPrefs.GetInt(levelName) <= 1){
				PlayerPrefs.SetInt(levelName, 1);
			}
			
		} else{
			StarCurrentLevel = 0;
			if(PlayerPrefs.GetInt(levelName) == 0){
				PlayerPrefs.SetInt(levelName, 0);
			}
		}

		UpdateLockCondition();
	}

	public void UpdateLockCondition(){
		string levelLockCondition = LevelDatabase.CurrentLevelLock;
		
		PlayerPrefs.SetInt(levelLockCondition,1);
	}
}
