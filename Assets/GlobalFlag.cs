﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalFlag : MonoBehaviour {

	public static bool LevelWin = false;
	public static bool isPlaying = false;
	private void Start() {
		isPlaying = true;
	}
	public static void ResetFlags(){
		LevelWin = false;
		isPlaying = false;
	}
	public static void Win(){
		LevelWin = true;
		isPlaying = false;
	}

	public void Pause(){
		LevelWin = false;
		isPlaying = false;
	}

	public void Resume(){
		LevelWin = false;
		isPlaying = true;
	}
}
