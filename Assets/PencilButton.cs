﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PencilButton : MonoBehaviour {

	public static bool isPencil;
	Image img;
	private void Start() {
		img = GetComponent<Image>();
	}
	public void ToggleClicked(){
		isPencil = !isPencil;
		if(isPencil){
			img.color = Color.red;
		} else{
			img.color = Color.white;
		}
		Debug.Log("isPencil: "+isPencil);
	}
}
