﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDetail : MonoBehaviour {

	public LevelDetail LevelToUnlock;
	public string LevelName;
	public string LevelLockPref;
	public string LevelNameInPopup;
	[TextArea]
	public string LevelDescription;
	[HideInInspector]
	//public int stars;
	public bool isLocked;
	public StarManager StarManager;
	private void Start() {
		UpdateLockCondition();
		StarManager.ChangeStar(PlayerPrefs.GetInt(LevelName));
		//Debug.Log("PlayerPrefs.GetInt(LevelName): "+PlayerPrefs.GetInt(LevelName));
	}

	void UpdateLockCondition(){
		Button btn = GetComponentInChildren<Button>();

		if(PlayerPrefs.GetInt(LevelLockPref) == 1){
			isLocked = false;
			btn.enabled = true;
		} else{
			isLocked = true;
			btn.enabled = false;
		}
	}

	public void SetLevelToPlay(){
		if(LevelToUnlock){
			LevelDatabase.CurrentLevelLock = LevelToUnlock.LevelLockPref;
		}
		LevelDatabase.CurrentLevel = LevelName;
		LevelSelectionPopUp.instance.LevelName.text = LevelNameInPopup;
		LevelSelectionPopUp.instance.LevelDescription.text = LevelDescription;
	}
}
