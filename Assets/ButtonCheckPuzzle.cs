﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCheckPuzzle : MonoBehaviour {

	public CellValueComparer CellValueComparer;
	[Tooltip("0: Classic, 1: FillNumber")]
	public int GameMode;
	public int LevelFillNumber;
	public void Check(){
		switch (GameMode)
		{
			case 0:
				CellValueComparer.CheckPuzzle(CellValueComparer.GameMode.Classic,0);
			break;
			case 1:
				CellValueComparer.CheckPuzzle(CellValueComparer.GameMode.FillNumber,LevelFillNumber);
			break;
			default:
			break;
		}
		
	}
}
