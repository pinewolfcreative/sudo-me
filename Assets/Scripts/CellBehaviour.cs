﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellBehaviour : MonoBehaviour {

	public static GameObject selectedCell;
	public static GameObject prevCell; 
	public static bool isColorDefined = false;
	public static Color normalColor;
	public bool IsPermanent = false;
	public int CorrectNumber;
	public Font Font;
	public Text cellText;
	Button btn;
	Image img;
	string tempString;
	public bool IsCorrect(){
		tempString = cellText.text;
		if(cellText.text == string.Empty){
			tempString = "0";
		}
		if(CorrectNumber == int.Parse(tempString)){
			return true;
		} else return false;
	}
	void Start(){
		cellText = GetComponentInChildren<Text>();
		cellText.font = GeneralReference.mainFont;
		cellText.rectTransform.anchoredPosition += new Vector2(0f,10);	//kasih offset kali ya?

		tempString = cellText.text;
		btn = GetComponent<Button>();
		img = GetComponent<Image>();
		if(!isColorDefined){
			normalColor = img.color;
			isColorDefined = true;
		}
        if (IsPermanent)
        {
            cellText.fontStyle = FontStyle.Bold;
        }

		btn.onClick.AddListener(ChangeColor);
	}
	void ChangeColor(){
		if(!IsPermanent){
			selectedCell = gameObject;
			if(prevCell == gameObject){
				selectedCell = null;
			}

			if(prevCell && prevCell != this){
				prevCell.GetComponent<Image>().color = normalColor; 
			}
			img.color = Color.red;
			
			if(prevCell == gameObject){
				img.color = normalColor;
				selectedCell = null;
				prevCell = null;
				return;
			}
			prevCell = gameObject;
		}
	}

}
