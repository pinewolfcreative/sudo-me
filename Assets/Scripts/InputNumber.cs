﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InputNumber : MonoBehaviour {

	public bool isDelete;
	Text myText;
	Button btn;
	PencilGridController pencilGrid;
	void Start(){
		myText = GetComponentInChildren<Text>();
		btn = GetComponent<Button>();
		if(isDelete){
			btn.onClick.AddListener(DeleteNum);
		}else{
			btn.onClick.AddListener(InputNum);
		}
	}
	public void InputNum(){
		if(!CellBehaviour.selectedCell){
			return;
		}
		GameObject selectedCell = CellBehaviour.selectedCell;
		Text selectedText = selectedCell.GetComponentInChildren<Text>();

		if(selectedCell && !selectedCell.gameObject.GetComponent<CellBehaviour>().IsPermanent){
			if(PencilButton.isPencil){
				selectedText.text = "";
				InputPencil();
			} else {
				DeletePencil();
				UndoSystem.instance.SetCellToUndo(selectedCell, selectedText.text);
				selectedText.text = myText.text;	
				if(selectedText.text != selectedCell.GetComponent<CellBehaviour>().CorrectNumber.ToString()){
					HealthController.instance.DecrementHealth();
				}
			}
		}
	}

	void InputPencil(){
		pencilGrid = CellBehaviour.selectedCell.GetComponentInChildren<PencilGridController>();
		pencilGrid.TogglePencil(int.Parse(myText.text));
	}

	void DeletePencil(){
		pencilGrid = CellBehaviour.selectedCell.GetComponentInChildren<PencilGridController>();
		pencilGrid.ResetPencil();
	}

	void DeleteNum(){
		if(CellBehaviour.selectedCell && !CellBehaviour.selectedCell.gameObject.GetComponent<CellBehaviour>().IsPermanent){
			if(PencilButton.isPencil){
				DeletePencil();
			}else{
				UndoSystem.instance.SetCellToUndo(CellBehaviour.selectedCell, CellBehaviour.selectedCell.GetComponentInChildren<Text>().text);
			
				CellBehaviour.selectedCell.GetComponentInChildren<Text>().text = "";	
			}
		}
	}
}
