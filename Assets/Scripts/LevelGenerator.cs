﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelGenerator : MonoBehaviour {

	public GameObject PuzzleRoot;
	public Backtrack Algorithm;
	public PuzzleWrapper Puzzle;
	GameObject[] Cell = new GameObject[9];
	int[] rnds = new int[9];
	void Start(){
		//GenerateSolvedPuzzle();
	}
	void SelectFirstRow(){
		int x = 0;

		int panel = 0;
		int child = 0;
		
		for (int j = 0; j < 3; j++)
		{
			child = 0;
			while (child >= 0 && child < 3)
			{
				//Debug.Log("panel, child: "+panel+" ,"+child);
				Cell[x] = PuzzleRoot.transform.GetChild(panel).GetChild(child).gameObject;
				x++;
				child++;
			}
			panel++;
		}
	}
	
	bool IsUnique(int num){
		for (int i = 0; i < 9; i++)
		{
			if(num == rnds[i]){
				return false;
			}
		}
		return true;
	}
	int GetRandom(){
		int x = Random.Range(1,10);
		while (!IsUnique(x))
		{
			x = Random.Range(1,10);
		}
		return x;
	}
	void RandomizeFirstRow(){
		for (int i = 0; i < 9; i++)
		{
			rnds[i] = 0;
		}
		for (int i = 0; i < 9; i++)
		{
			rnds[i] = GetRandom();
			Cell[i].GetComponentInChildren<Text>().text = rnds[i].ToString();
			Cell[i].GetComponent<CellBehaviour>().CorrectNumber = rnds[i];
		}
	}
	public void GenerateSolvedPuzzle(){
		Algorithm.ClearSudoku();
		SelectFirstRow();
		RandomizeFirstRow();
		Algorithm.Solve();
		HideSomeCells();
	}

	void HideSomeCells(){
		int rnd;
		for (int i = 0; i < 81; i++)
		{
			rnd = Random.Range(0,2);
			if(rnd == 0){
				Puzzle.Cell[i].GetComponentInChildren<Text>().text = string.Empty;
			}
		}
		for (int i = 0; i < 81; i++)
		{
			if(Puzzle.Cell[i].GetComponentInChildren<Text>().text != string.Empty){
				Puzzle.Cell[i].GetComponent<CellBehaviour>().IsPermanent = true;
			}
		}
	}
}
