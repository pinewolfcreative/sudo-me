﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolvedChecker : MonoBehaviour {
	public GameObject[] Cell = new GameObject[81];
	[HideInInspector]
	public CellObject[] CellObjects = new CellObject[81];
	void Start(){
		
	// 	for (int i = 0; i < 81; i++)
	// 	{
	// 		if(i/9 == 0){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 1){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 2){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 3){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 4){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 5){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 6){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 7){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 		if(i/9 == 8){
	// 			Debug.Log("i: "+i+", row: "+(i/9));
	// 		}
	// 	}
	}
	
}

public class CellObject{
	public int Row;
	public int Col;
	public int Box;
	public void SetCell(int row, int col, int box){
		Row = row;
		Col = col;
		Box = box;
	}
}
