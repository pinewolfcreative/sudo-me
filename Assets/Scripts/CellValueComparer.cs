﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellValueComparer : MonoBehaviour {

	public enum GameMode{Classic,FillNumber};
	public GameObject ResultPanel;
	public List<CellBehaviour> CellBehaviours;
	public GameObject PuzzleRoot;
	StarManager starManager;
	void Start () {
		PuzzleRoot.GetComponentsInChildren<CellBehaviour>(true,CellBehaviours);
		starManager = ResultPanel.GetComponent<StarManager>();
	}
	
	
	public void CheckPuzzle(GameMode gameMode, int number){
		bool isWin;

		switch (gameMode)
		{
			case GameMode.Classic:
				isWin = IsPuzzleCompleted();
			break;
			case GameMode.FillNumber:
				isWin = IsNumberCompleted(number);
			break;

			default:
			isWin = false;
			break;
		}

		if(isWin){
			ScoreManager.instance.UpdateScore();
			ResultPanel.SetActive(true);
			starManager.ChangeStar(ScoreManager.StarCurrentLevel);

			GlobalFlag.Win();
		}else{
			//Debug.Log("Not Solved");
			PopUpText.Instance.ShowPopUp("Puzzle not solved yet!");
		}
	}

	#region ClassicCompletePuzzle
	bool IsPuzzleCompleted(){
		foreach (var item in CellBehaviours)
		{
			if(!item.IsCorrect()){
				return false;
			}
		}
		return true;
	}
	#endregion
	
	#region CompleteCertainNumber
	bool IsNumberCompleted(int number){
		foreach (var item in CellBehaviours)
		{
			if(item.CorrectNumber == number){
				if(!item.IsCorrect()){
					return false;
				}
			}
		}
		return true;
	}
	#endregion
}
