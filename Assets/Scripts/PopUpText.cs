﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpText : MonoBehaviour {

// nanti pake lerp untuk show dan hidenya
public static PopUpText Instance;
public GameObject PopUpPanel;
public Text text;
public float PopUpTime;
float timecounter;

void Start(){
}
bool isShowing;
	void Awake(){
		Instance = this;
		timecounter = 0;
	}

	void Update(){
		if(isShowing){
			timecounter -= Time.deltaTime;
			if(timecounter <= 0){
				Hide();
				isShowing = false;
			}
		}
	}

	public void ShowPopUp(string Text){		// ini yang dipake
		PopUpPanel.SetActive(true);
		text.text = Text;
		isShowing = true;
		timecounter = PopUpTime;
	}
	void Hide(){
		PopUpPanel.SetActive(false);
		text.text = string.Empty;
	}
}
