﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class Backtrack : MonoBehaviour
{
    public int PuzzleRow; // default 9
    public int PuzzleCol; // default 9
    public int CellPerCol; // default 3
    public int CellPerRow; // default 3
    private int PanelRow; // kayanya ga butuh
    private int PanelCol; // kayanya ga butuh
    public int[,] grid;
    private int[,] gridCheck;
    public Color FilledColor;
    public Color TextColor;
	public PuzzleWrapper Puzzle;
    public Cell[] Cell = new Cell[81];

    private void Start()
    {
        grid = new int[PuzzleRow, PuzzleCol];
        gridCheck = new int[PuzzleRow, PuzzleCol];

        PanelCol = PuzzleCol/CellPerCol;
        PanelRow = PuzzleRow/CellPerRow;

        for(int i=0; i<81; i++){
            
            Puzzle.Cell[i].gameObject.transform.GetComponentInChildren<Text>().supportRichText = false;
            Cell[i] = new Cell();
        }
            
 
        for (int i = 0; i < PuzzleRow; i++)
            for (int j = 0; j < PuzzleCol; j++)
                grid[i,j] = 0;
    }
 
    public void ClearSudoku()
    {
        for (int i = 0; i < PuzzleRow*PuzzleCol; i++){
            Puzzle.Cell[i].gameObject.GetComponentInChildren<Text>().text = null;
            Puzzle.Cell[i].gameObject.GetComponentInChildren<CellBehaviour>().IsPermanent = false;
        }
    }
    
#region Solver
    public void Solve()
    {
		if(CheckIfAllFilled()){
			//Debug.Log("Already solved");
			return;
		}
        int x = 0;
        for(int i=0; i<PuzzleRow; i++)
        {
            for(int j=0; j<PuzzleCol; j++)
            {
                Text str = Puzzle.Cell[x].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
 
                if (str.text != string.Empty)
                {
                    //ColorBlock cb = gameObject.transform.GetChild(x).gameObject.GetComponent<InputField>().colors;
                    //cb.normalColor = FilledColor;
                    //gameObject.transform.GetChild(x).gameObject.GetComponent<InputField>().colors = cb;
                    //Puzzle.Cell[x].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color = TextColor;
                    grid[i, j] = int.Parse(str.text);
                }            
                else
                {
 
                    grid[i, j] = 0;
                }
                   
                x++;
            }
        }
 
        if (SolveSudoku(grid))
            PrintGrid(ref grid);
        else{
            //Debug.Log("!No Solution Exists");
        }
    }
    
	private bool CheckIfAllFilled(){
		for (int i = 0; i < 81; i++)
		{
			if(Puzzle.Cell[i].gameObject.GetComponentInChildren<Text>().text == ""){
				//Debug.Log("ada yang kosong");
				return false;
			}
		}
		return true;
	}

    private void PrintGrid(ref int[,] grid)
    {
        int[] tempGrid = new int[81];
        int x = 0;
        for(int i=0; i<PuzzleRow; i++)
        {
            for(int j=0; j<PuzzleCol; j++)
            {
                tempGrid[x] = grid[i,j];
                x++;
            }
        }
        //StartCoroutine(Output(tempGrid));
		Output(tempGrid);
    }
	private void Output(int[] grid){
			for(int x=0; x<PuzzleRow*PuzzleCol; x++){
				if(Puzzle.Cell[x].GetComponentInChildren<Text>().text == string.Empty){
					for	(int i = 0; i<=grid[x]; i++){
						Puzzle.Cell[x].GetComponentInChildren<Text>().text = i.ToString();
                        Puzzle.Cell[x].GetComponent<CellBehaviour>().CorrectNumber = i; //test dulu
                        //yield return new WaitForSeconds(1);
                    }
				}
				
			}
		}
    private bool FindUnassignedLocation(int[,] grid, ref int row, ref int col)
    {
        for (row = 0; row < PuzzleRow; row++)
            for (col = 0; col < PuzzleCol; col++)
                if (grid[row, col] == 0)
                    return true;
        return false;
    }
 
    private bool UsedInRow(int[,] grid, int row, int num)
    {
        for (int col = 0; col < PuzzleCol; col++)
            if (grid[row, col] == num)
                return true;
        return false;
    }
 
    private bool UsedInCol(int[,] grid, int col, int num)
    {
        for (int row = 0; row < PuzzleRow; row++)
            if (grid[row, col] == num)
                return true;
        return false;
    }
 
    private bool UsedInBox(int[,] grid, int BoxStartRow, int BoxStartCol, int num)
    {
        for(int row=0; row<CellPerRow; row++)
            for (int col = 0; col < CellPerCol; col++)
                if (grid[row+BoxStartRow, col+BoxStartCol] == num)
                    return true;
        return false;
    }

    private bool isSafe(int[,] grid, int row, int col, int num)
    {
        return !UsedInRow(grid, row, num)
            && !UsedInCol(grid, col, num)
            && !UsedInBox(grid, row-row%CellPerRow, col-col%CellPerCol, num);
    }
 
    private bool SolveSudoku(int[,] grid)
    {
        int row = new int();
        int col = new int();
 
        if (!FindUnassignedLocation(grid, ref row, ref col))
            return true;
       
        for(int num=1; num<=9; num++)
        {
            if(isSafe(grid, row, col, num))
            {
                grid[row, col] = num;
                if (SolveSudoku(grid))
                    return true;
                grid[row, col] = 0;
            }
        }
 
        return false;
    }
#endregion
    

#region Checker
public void Check()
    {
        int x = 0;
        for(int i=0; i<PuzzleCol; i++)
        {
            for(int j=0; j<PuzzleRow; j++)
            {
                Text str = Puzzle.Cell[x].gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
 
                if (str.text != string.Empty)
                {
                    gridCheck[i, j] = int.Parse(str.text);
                    Cell[x].SetCell(i,j,gridCheck[i, j]);
                }            
                else
                {
                    Debug.Log("There is still empty cell!");
                    return;
                }
                   
                x++;
            }
        }
 
        for (int i = 0; i < PanelCol*PanelRow; i++)
        {
            if(!isSafeCheck(gridCheck, Cell[i].Row, Cell[i].Col, Cell[i].Num)){
                Debug.Log("Puzzle not filled correctly!");
                Debug.Log(i+","+Cell[i].Row+","+ Cell[i].Col+","+ Cell[i].Num);
                return;
            }
        }
        Debug.Log("Puzzle solved!");
    }
    private bool UsedInRowCheck(int[,] grid, int row, int num, int cellCol)
    {
        for (int col = 0; col < PuzzleCol; col++){
            if(cellCol == col){
                continue;
            }
            if (grid[row, col] == num)
                return true;
        }
            
        return false;
    }
 
    private bool UsedInColCheck(int[,] grid, int col, int num, int cellRow)
    {
        for (int row = 0; row < PuzzleRow; row++){
            if(cellRow == row){
                continue;
            }
            if (grid[row, col] == num)
                return true;
        }
        return false;
    }
 
    private bool UsedInBoxCheck(int[,] grid, int BoxStartRow, int BoxStartCol, int num, int cellRow, int cellCol)
    {
        for(int row=0; row<CellPerRow; row++){
            for (int col = 0; col < CellPerCol; col++){
                if(cellRow == row+BoxStartRow && cellCol == col+BoxStartCol){
                    continue;
                }
                if (grid[row+BoxStartRow, col+BoxStartCol] == num){
                    return true;
                }
            }
        }
        return false;
    }
 
    private bool isSafeCheck(int[,] grid, int row, int col, int num)
    {
        return !UsedInRowCheck(grid, row, num, col)
            && !UsedInColCheck(grid, col, num, row)
            && !UsedInBoxCheck(grid, row-row%3, col-col%3, num, row, col);
    }
#endregion

    
}

public class Cell{
    public int Row;
    public int Col;
    public int Num;
    public void SetCell(int row, int col, int num){
        Row = row;
        Col = col;
        Num = num;
    }
}