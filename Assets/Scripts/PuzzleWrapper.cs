﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleWrapper : MonoBehaviour {

	public GameObject PuzzleRoot;
	public GameObject[] Cell = new GameObject[81];

	void Awake(){
		//WrapPuzzle();
	}

	void WrapPuzzle(){
		int x = 0;

		int panel = 0;
		int child = 0;
		int panel_a = 3; 
		int panel_b = 0;
		int child_a = 3; 
		int child_b = 0;
		
		for (int i = 0; i < 3; i++)
		{
			while (panel >= panel_b && panel < panel_a)
			{
				for (int k = 0; k < 3; k++)
				{
					panel = panel_b;
					for (int j = 0; j < 3; j++)
					{
						child = child_b;
						while (child >= child_b && child < child_a)
						{
							//Debug.Log("panel, child: "+panel+" ,"+child);
							if(Cell[x]){
								Cell[x] = PuzzleRoot.transform.GetChild(panel).GetChild(child).gameObject;
								x++;
								child++;
							}
						}
						panel++;
					}
					child_a += 3;
					child_b += 3;
				}
			}
			child_a = 3;
			child_b = 0;
			panel_a += 3;
			panel_b += 3;
		}
	}
}

	