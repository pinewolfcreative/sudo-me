﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDataWrapper : MonoBehaviour {
	public static LevelDataWrapper instance;

	LevelDatabase levelDatabase;
	GameObject level;

	private void Awake() {
		instance = this;
		// if(instance == null){
		// 	instance = this;
		// 	DontDestroyOnLoad(this);
		// }
		// if(instance != this){
		// 	Destroy(gameObject);
		// }
	}
	private void Start() {
		levelDatabase = LevelDatabase.instance;
		//WrapLevels();
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.P)){
			WrapLevels();
			Debug.Log("Level Wrapped");
		}
	}
	public void WrapLevels(){
		LevelDetail levelDetail;
		StarManager starManager;
		foreach (var item in levelDatabase.LevelList)
		{
			level = item.LevelObject;
			levelDetail = level.GetComponent<LevelDetail>();
			starManager = level.GetComponentInChildren<StarManager>();

			starManager.ChangeStar(PlayerPrefs.GetInt(levelDetail.LevelName));
			// if(LevelDatabase.CurrentLevel == levelDetail.LevelName){
			// 	starManager.ChangeStar(PlayerPrefs.GetInt("CurrentLevelStar")); //wrapping stars dari database ke GameObject "LevelSelection"
			// 	PlayerPrefs.SetInt("CurrentLevelStar", 0);
			// }
		}
	}
}
