﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDatabase : MonoBehaviour {

	public GameObject LevelCanvas;
	public static LevelDatabase instance;
	public static string CurrentLevel;
	public static string CurrentLevelLock;

	[HideInInspector]
	public List<Level> LevelList;
	List<LevelDetail> LevelDetails = new List<LevelDetail>();
	private void Awake() {
		instance = this;
		PlayerPrefs.SetInt("LevelLock1",1);

	}
	private void Start() {
		if(SceneManager.GetCurrentSceneName() == "03Option"){
			return;
		}
		WrapLevels();
	}

	void WrapLevels(){
		LevelCanvas.GetComponentsInChildren<LevelDetail>(LevelDetails);
		int length = LevelDetails.Count;
		for (int i = 0; i < length; i++)
		{
			LevelList[i].SetObject(LevelDetails[i].gameObject);
		}
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.R)){
			ResetPlayerPref();
		}
	}
	public void ResetPlayerPref(){
		int rangex = LevelList.Count;
		//Debug.Log("range: "+ rangex);
		for (int i = 1; i <= rangex; i++)
		{
			PlayerPrefs.SetInt("Level"+i,0);
			PlayerPrefs.SetInt("LevelLock"+i,0);
		}
	}

	public void ResetPlayerPref(int JumlahLevel){
		for (int i = 1; i <= JumlahLevel; i++)
		{
			PlayerPrefs.SetInt("Level"+i,0);
			PlayerPrefs.SetInt("LevelLock"+i,0);
		}
	}
}

[System.Serializable]
public struct Level{
	public GameObject LevelObject;
	public void SetObject(GameObject levelObject){
		LevelObject = levelObject;
	}
}