﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour {

	public Text MinuteTxt;
	public Text SecondTxt;
	float counter = 0;
	int minute = 0;
	int second = 0;
	[SerializeField]

	private void Start() {
		MinuteTxt.text = "00";
		SecondTxt.text = "00";
	}
	private void Update() {
		if(GlobalFlag.isPlaying){
			CheckSeconds();
		}
	}

    void CheckSeconds(){
		counter += Time.deltaTime;
		second = (int)counter;
		if(second < 10){
				SecondTxt.text = ("0" + second.ToString());
			}else{
				SecondTxt.text = (second.ToString());
			}
		
		if(counter > 60){
			minute++;
			SecondTxt.text = "00";
			if(minute < 10){
				MinuteTxt.text = ("0" + minute.ToString());
			}else{
				MinuteTxt.text = (minute.ToString());
			}
			counter = 0;
		}
	}
}
