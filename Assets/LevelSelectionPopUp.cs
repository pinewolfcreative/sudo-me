﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelSelectionPopUp : MonoBehaviour {

	public static LevelSelectionPopUp instance;
	public Text LevelName;
	public Text LevelDescription;

	private void Awake() {
		instance = this;
	}
}
