﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoSystem : MonoBehaviour {

	public static UndoSystem instance;
	public GameObject CellGameObject;
	CellBehaviour PreviousCell;
	public string PreviousNumber;

	private void Awake() {
		instance = this;
	}
	//nanti diatur dari cell behaviour yang terakhir diinput player (si PreviousCell && PreviousNumber)
	public void Undo(){
		if(PreviousCell){
			PreviousCell.cellText.text = PreviousNumber;
		}
	}

	public void SetCellToUndo(GameObject Cell, string Number){
		PreviousCell = Cell.GetComponent<CellBehaviour>();
		PreviousNumber = Number;
	}
}
