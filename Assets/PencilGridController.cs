﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PencilGridController : MonoBehaviour {

	public GameObject[] Pencils = new GameObject[9];

	public void TogglePencil(int number){
		Pencils[number-1].SetActive(!Pencils[number-1].activeSelf);
	}

	public void ResetPencil(){
		for (int i = 0; i < 9; i++)
		{
			Pencils[i].SetActive(false);
		}
	}
}
