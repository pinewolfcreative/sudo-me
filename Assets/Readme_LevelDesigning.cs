﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Readme_LevelDesigning : MonoBehaviour {

	[Header("Hover mouse ke tiap item")]
	[Tooltip("Masukin  prefab 'SudokuCell' sebagai childnya 'Puzzle, dengan begitu cell yang baru ini akan diperhitungkan ketika solving puzzle")]
	public bool CaraNgisiPuzzle;
	[Tooltip("Bisa juga tinggal atur2 posisi dan jumlah dari cell2 yang udah ada di child 'Puzzle', mereka udah kebaca juga sama kedua metode checking")]
	public bool EditExistingCell;
	[Tooltip("Bedanya, kalo check(CV) itu compare value kebenaran dari angka yang ada di cell, sedangkan check (BT) pakenya algoritma untuk membandingkan kondisi tiap cell dengan aturan2 dalam sudoku")]
	public bool CheckCvCheckBt;
    [Tooltip("Slider di kiri atas buat deteksi jumlah star. nanti kelink sama timer biar bisa nentuin bintang berapa.")]
    public bool SliderInLeft;
    // open for another notes

}
