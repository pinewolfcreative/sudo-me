﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour {

	public static HealthController instance;
	public static int HealthPoint;
	public int MaxHealth;
	public Text Health;
	public GameObject LosePanel;
	
	private void Awake() {
		instance = this;
	}
	void Start () {
		HealthPoint = MaxHealth;
		UpdateHealth();
	}

	public void DecrementHealth(){
		HealthPoint--;
		if(HealthPoint <= 0){
			LosePanel.SetActive(true);
		}
		PopUpText.Instance.ShowPopUp("Wrong number!");
		UpdateHealth();
	}
	public void UpdateHealth(){
		Health.text = HealthPoint.ToString();
	}
}
