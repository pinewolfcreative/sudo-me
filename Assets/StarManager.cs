﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarManager : MonoBehaviour {

	public Image Star1;
	public Image Star2;
	public Image Star3;
	public Sprite YellowStar;
	public Sprite WhiteStar;

	// [Space]
	// public int testStar;
	void Start () {
	}
	public void ChangeStar(int score){
		switch (score)
		{
			case 0:
				Star1.sprite = WhiteStar;
				Star2.sprite = WhiteStar;
				Star3.sprite = WhiteStar;
			break;
			case 1:
				Star1.sprite = YellowStar;
				Star2.sprite = WhiteStar;
				Star3.sprite = WhiteStar;
			break;
			case 2:
				Star1.sprite = YellowStar;
				Star2.sprite = YellowStar;
				Star3.sprite = WhiteStar;
			break;
			case 3:
				Star1.sprite = YellowStar;
				Star2.sprite = YellowStar;
				Star3.sprite = YellowStar;
			break;
			default:
			Debug.Log("score invalid!");
			break;
		}
	}
}
